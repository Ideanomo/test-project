var gulp = require('gulp'),
sass = require('gulp-sass'),// wrapper from gulp to sass...compile stylesheets
browserSync = require('browser-sync'), // synchronised browser testing and live reloading
gutil = require('gulp-util'),  //
useref = require('gulp-useref'),
gulpIf = require('gulp-if'),
uglify = require('gulp-uglify'), // concatenate and minify js
cssnano = require('gulp-cssnano'),  // minifies css
notify = require('gulp-notify'),
beep = require('beepbeep');  // make console beep sound.


// Define default task and log a message for sake of practice.
gulp.task('default', function () {
    gutil.log('Gulp is', 'running', gutil.colors.yellow(':)')); // Log stuff and joins multiple arguments by a space. Uses default gulp colouring
});


//Define default task that builds entire project
/*
gulp.task('default', 'useref', 'watch');
*/

// Compile Sass to CSS using gulp-sass plugin
// gulp's pipe() function calls on each plugin individually.And works like physical pipes
//  So they work as normal pipes, feeding one thing into the first pipe, which
// then passes through every connected pipe

    gulp.task('sass', function() {  // gulp.task() defines a new task 'sass' that runs a js function whenever the 'sass task is run.
        return gulp.src('app/scss/styles.scss')
        .pipe(sass())  // First pipe compiles sass into css
        .pipe(gulp.dest('app/css/'))  // connect a second pipe  that takes this css and puts it into app/css,
        .pipe(browserSync.reload({   // connect a third pipe that loads that css into the browser that browserSync creates.
            stream: true
        }))
  });

// Create browserSync task so Gulp can create a server using Browser Sync.
gulp.task('browserSync', function() {
    browserSync
      ({
        server:
          {
              baseDir: 'app' // Tells Browser Sync where the root of the server should be.
          }
       })
  });

// Group together multiple watch processes into a watch task
// Watches for .scss files in root and child directories
gulp.task('watch', ['sass', 'browserSync'], function() {
        gulp.watch('app/scss/**/*.scss', ['sass']);
        // Reloads the browser whenever HTML or JS files change
        gulp.watch('app/*.html', browserSync.reload);
        gulp.watch('app/js/**/*.js', browserSync.reload);
  });

// Setup the useref task to look in html for build comments then uglify (concatenate and minify js and minify css using cssnano
gulp.task('useref', function() {
    return gulp.src('app/*.html')
        .pipe(useref())
        // Minifies only if it's a JS file
        .pipe(gulpIf('*.js', uglify()))
        // Minifies only if it's a CSS file
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('dist'))
        .pipe(notify({message: 'minification/concatenation complete'}));
  });



